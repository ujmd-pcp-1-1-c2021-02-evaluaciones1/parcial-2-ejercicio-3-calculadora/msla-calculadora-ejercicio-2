﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSLA_CALCULADORA_EJERCICIO_2
{
    public partial class Form1 : Form
    {
        double resulado = 0;
        string operacion = "suma";
        
        public Form1()
        {
            InitializeComponent();
            label3.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out resulado ) & double.TryParse(textBox2.Text, out resulado))
            { 

          if (operacion == "suma")
                {
                resulado = double.Parse(textBox1.Text) + double.Parse(textBox2.Text);
                label3.Text = resulado.ToString();
                label3.Visible = true;
                }
            else if (operacion == "resta")
            {
                resulado = double.Parse(textBox1.Text) - double.Parse(textBox2.Text);
                label3.Text = resulado.ToString();
                label3.Visible = true;
            }
            else if (operacion == "multiplicacion")
            {
                resulado = double.Parse(textBox1.Text) * double.Parse(textBox2.Text);
                label3.Text = resulado.ToString();
                label3.Visible = true;
            }
            else if (operacion == "divicion")
            {
                resulado = double.Parse(textBox1.Text) / double.Parse(textBox2.Text);
                label3.Text = resulado.ToString();
                label3.Visible = true;
            }
           }
            else
            {
                MessageBox.Show("Favor ingresar datos correctos o completar datos");
                label3.Text = "SyntaxError";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            operacion = "divicion";

        }

        private void button3_Click(object sender, EventArgs e)
        {
            operacion = "suma";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            operacion = "resta";

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            operacion = "multiplicacion";

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
